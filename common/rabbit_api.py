"""Utils for connecting to RabbitMQ."""
import pika


class RabbitAPI:
    def __init__(self, host, user, password, queue):
        self._conn = pika.BlockingConnection(pika.ConnectionParameters(
            host=host,
            credentials=pika.PlainCredentials(user, password)
        ))
        self._channel = self._conn.channel()
        self._queue = queue

        self._setup_channel()
        self._setup_queue(queue)

    def _setup_channel(self):
        self._channel.basic_qos(prefetch_count=1)

    def _setup_queue(self, queue, durable=True):
        self._channel.queue_declare(queue=queue, durable=durable)

    def close(self):
        self._conn.close()

    def send(self, msg, exchange='', routing_key=''):
        self._channel.basic_publish(
            exchange=exchange,
            routing_key=routing_key or self._queue,
            body=msg,
            properties=pika.BasicProperties(
                delivery_mode=2,  # make message persistent
            )
        )

    def receive_using(self, callback):
        self._channel.basic_consume(callback, queue=self._queue)
        self._channel.start_consuming()

    def receive_single(self, raw=False):
        method, header, body = next(self._channel.consume(queue=self._queue))

        if method:
            self._channel.basic_ack(delivery_tag=method.delivery_tag)

        self._channel.cancel()

        if raw:
            return method, header, body

        return body


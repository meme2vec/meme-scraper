# meme-scraper

This project is based on the Scrapy framework. To get started, install Scrapy
locally or use a virtualenv:

```bash
. /path/to/venv/bin/activate
cd /path/to/the/meme-language/repository
pip install -r requirements.txt
``` 

Next, you should start a *RabbitMQ* server:
```bash
$ docker-compose up  # Starts containers described in compose file, specifically RabbitMQ container
```

There are two kinds of scrapers right now: **scrapy** based and **praw** based (for *Reddit*).

If you want to run a scrapy scraper (*9gag* in this example) for chosen category (`${category}`):
```bash
$ scrapy crawl 9gag -a category=${category}  # Processes 9gag api requests and puts them onto the '9gag' queue
```

*memegenerator* spider example:
```bash
scrapy crawl memegenerator -a api_key=<API-KEY> -o output.json
```  

As for *Reddit* scraper, make sure to input valid credentials in `reddit_scraper/config.py` and run:
```bash
python -m reddit_scraper -h
```
to list available parameters.

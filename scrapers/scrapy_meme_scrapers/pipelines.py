# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from celery import group
from scrapy.exceptions import DropItem

from common import celery_common as cc, db_api


class MongoWriterPipeline:
    def open_spider(self, spider):
        self._celery_app = cc.make_app('spider')
        self._mongo_api = db_api.StatefulMongoAPI()

    def close_spider(self, spider):
        self._celery_app.close()
        self._mongo_api.close()

    def process_item(self, item, spider):
        if self._mongo_api.get('instanceUrl', item['instanceUrl']):
            raise DropItem('Already in database.')

        mongo_id = self._mongo_api.save(item)

        rescraping_sig = self._celery_app.signature(
            'rescraping_task',
            args=(mongo_id, spider.name),
            kwargs={'initial_call': True},
            queue='rescraping'
        )

        sig_to_run = rescraping_sig

        if not item['text']:
            ocr_sig = self._celery_app.signature(
                'OcrImageFromUrlTask',
                args=(mongo_id,),
                queue='ocr'
            )

            sig_to_run = group(ocr_sig, rescraping_sig)

        sig_to_run.apply_async()

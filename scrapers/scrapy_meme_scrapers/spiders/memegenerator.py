import json

import scrapy

from common.data_model import DataModel


class MemeGeneratorNet(scrapy.Spider):
    name = 'memegenerator'

    ORIGIN_URL_TEMPLATE = \
        'http://version1.api.memegenerator.net//Instances_Select_ByPopular?' \
        'languageCode={}&pageIndex={}&apiKey={}'

    def __init__(self, api_key, starting_index=0, language='en', **kwargs):
        super().__init__(**kwargs)

        self.api_key = api_key
        self.current_index = int(starting_index)
        self.language = language

        self.start_urls = [self._format_url()]

    def parse(self, response):
        res = json.loads(response.text)
        data = res['result']

        if len(data) == 0:
            return

        for entry in data:
            meme = DataModel()

            (meme
             .set_portal_id(MemeGeneratorNet.name)
             .set_instance_url(entry['instanceUrl'])
             .set_image_template(entry['displayName'])
             .set_image_url(entry['instanceImageUrl'])
             .set_text([entry['text0'], entry['text1']])
             .set_num_points(entry['totalVotesScore'])
             .set_num_comments(entry['commentsCount'])
             .set_user_id(entry['username'])
             )

            yield meme.commit_and_return_dict()

        self.current_index += 1
        yield scrapy.Request(self._format_url(), callback=self.parse)

    def _format_url(self):
        return self.ORIGIN_URL_TEMPLATE.format(self.language,
                                               self.current_index,
                                               self.api_key)

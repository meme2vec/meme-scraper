# -*- coding: utf-8 -*-
import json

import scrapy

from common.data_model import DataModel


class NineGagSpider(scrapy.Spider):
    name = '9gag'
    allowed_domains = ['9gag.com']

    ORIGIN_URL_TEMPLATE = 'https://9gag.com/v1/group-posts/group/default/type/'

    def __init__(self, category, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._origin_url = self.ORIGIN_URL_TEMPLATE + category
        self.start_urls = [self._origin_url]

    def parse(self, response):
        res = json.loads(response.text)
        data = res['data']

        if not data.get('nextCursor'):
            return  # end of stream

        for post in data['posts']:
            if post['type'] != 'Photo':
                continue

            points = post['upVoteCount'] - post['downVoteCount']

            meme = DataModel()
            (meme
             .set_portal_id(NineGagSpider.name)
             .set_instance_url(post['url'])
             .set_num_points(points)
             .set_num_comments(post['commentsCount'])
             .set_image_url(post['images']['image700']['url'])
            )

            yield meme.commit_and_return_dict()

        next_url = self._origin_url + '?' + data['nextCursor']
        yield scrapy.Request(next_url, callback=self.parse)

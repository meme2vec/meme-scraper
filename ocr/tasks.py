"""Celery tasks definitions for OCR workers."""
from celery.exceptions import TaskError

from common import db_api
from common.data_model import DataModel
from ocr.celery import app
from ocr.ocr_app import image_ocr as ocr


@app.task(name='OcrImageFromUrlTask')
def run_ocr(meme_id):
    mongo_api = db_api.StatelessMongoAPI()
    meme_object = mongo_api.get_by_id(meme_id)

    if not meme_object:
        raise TaskError(f"Object with id={meme_id} not found")

    text_lines = ocr.load_image_and_run_ocr(meme_object['imageUrl'],
                                            from_url=True)

    if len(text_lines) > 0:
        new_meme = (DataModel(meme_object)
                    .set_text(text_lines)
                    .commit_and_return_dict())

        mongo_api.replace_by_id(str_id=meme_id, new_meme=new_meme)

    return meme_id

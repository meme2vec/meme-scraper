from io import BytesIO
import re

import numpy as np
from PIL import Image
import pytesseract
import requests


def load_image_and_run_ocr(image_path, from_url=False):
    if from_url:
        response = requests.get(image_path)
        img = Image.open(BytesIO(response.content))
    else:
        img = Image.open(image_path)

    img = np.array(img)

    return _run_ocr(img)


def _run_ocr(img, threshold=253, min_chars=3):
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if all([elem >= threshold for elem in img[i][j]]):
                img[i][j] = (255, 255, 255)
            else:
                img[i][j] = (0, 0, 0)

    text = pytesseract.image_to_string(Image.fromarray(img), lang='meme')

    cleaned_lines = []

    for block in text.split('\n\n'):
        ws_cleaned = re.sub(r'\s+', ' ', block.strip())
        non_alp_num_cleaned = re.sub('[^0-9a-zA-Z\\s?!-]+', '',
                                     ws_cleaned).strip()

        if len(non_alp_num_cleaned) > min_chars:
            cleaned_lines.append(non_alp_num_cleaned)

    return cleaned_lines

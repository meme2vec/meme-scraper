"""Celery tasks definitions for DB workers."""
from common import db_api
from db.celery import app


@app.task(name='DBSaveTask')
def save(item):
    return db_api.StatelessMongoAPI().save(item)
